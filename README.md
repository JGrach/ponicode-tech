# Ponicode test tech

## Instructions
https://www.codingame.com/ide/puzzle/roller-coaster

## Launch

`git clone git@gitlab.com:JGrach/ponicode-tech.git && cd ponicode-tech && npm i && npm run dev`

You may choose your file with argument e.g:

`npm run dev src/db/roller_coaster.harder`

or:

`npm run dev src/db/roller_coaster.hard`

Tests are availables:

`npm run test`

## Bowlerplate

`https://gitlab.com/JGrach/node-typescript`

