import { addToWagon, observeWagon, countPeoples } from './counter'
import { expect } from 'chai'


describe("addToWagon", () => {
    it("very short queue", () => {
        const capacity = 10;
        const queue = [1];

        const add = addToWagon(capacity, queue);
        const res = add();

        expect(res.sum).equal(1)
        expect(res.nextGroupI).equal(0)
    })
    it("capacity easy", () => {
        const capacity = 5;
        const queue = [2,3,1,2,4];

        const add = addToWagon(capacity, queue);
        const res = add();

        expect(res.sum).equal(5)
        expect(res.nextGroupI).equal(2)
    })

    it("capacity middle", () => {
        const capacity = 5;
        // cycle : (2 - 3) - (1 - 2) - (4 - 1) - (3 - 2) - (2 - 4) - (1 - 2) - (3 - 1) - 2 - (4 - 1) - (3 - 2) 
        const queue = [2,3,1,2,4,1,3,2,2,4,1];

        const add = addToWagon(capacity, queue);
        const res = add();

        expect(res.sum).equal(5)
        expect(res.nextGroupI).equal(2)
    })
})


describe("observeWagon", () => {
    it("easy", () => {
        const capacity = 5;
        const roundMax = 10;
        const queue = [2, 5, 3];

        const res = observeWagon(capacity, roundMax, queue);

        // cycle : 2 - 5 - (3 - 2) - 5 - (3 - 2) - 5 - (3 - 2) - 5 - (3 - 2) - 5
        expect(res.cycleBeginAt).equal('1-5'); // indice 1 - sum 5
        expect(res.sum).equal(12); // sum for shift + first cycle
        expect(res.iteration).equal(3); // iteration for shift + first cycle
    })
    it("just one", () => {
        const capacity = 5;
        const roundMax = 10;
        const queue = [2];

        const res = observeWagon(capacity, roundMax, queue);

        expect(res.cycleBeginAt).equal('0-2');
        expect(res.sum).equal(2);
        expect(res.iteration).equal(1);
    }),
    it("harder than easy", () => {
        const capacity = 5;
        const roundMax = 15;
        const queue = [2,3,1,2,4,1,3,2,2,4,1,3];

        const res = observeWagon(capacity, roundMax, queue);

        // cycle : (2 - 3) - (1 - 2) - (4 - 1) - (3 - 2) - 2 - (4 - 1) - (3 - 2) - (3 - 1) - 2 - (4 - 1) - (3 - 2) - 2 - (4 - 1) - (3 - 2) - (3 - 1)
        expect(res.cycleBeginAt).equal('4-5'); // indice 4 - sum 5
        expect(res.sum).equal(36); // sum for shift + first cycle
        expect(res.iteration).equal(9); // iteration for shift + first cycle
    })
})

describe("countPeoples", () => {
    it("easy", () => {
        const capacity = 5;
        const roundMax = 10;
        const observation = {
            cycleBeginAt: '1-5',
            sum: 12,
            iteration: 3,
            wagons: {
                '0-2': { 
                    iterationCounter: 1, 
                    sumCounter: 2, 
                    wagon: {
                        sum: 2,
                        nextGroupI: 1
                    } 
                },
                '1-5': { 
                    iterationCounter: 2, 
                    sumCounter: 7, 
                    wagon: {
                        sum: 5,
                        nextGroupI: 2
                    }  
                },
                '2-5': { 
                    iterationCounter: 3, 
                    sumCounter: 12, 
                    wagon: {
                        sum: 5,
                        nextGroupI: 1
                    }
                }
            }
        };

        const res = countPeoples(roundMax, capacity, observation);

        expect(res).equal(47);
    })
    it("just one", () => {
        const capacity = 5;
        const roundMax = 10;
        const observation = {
            cycleBeginAt: '0-2',
            sum: 2,
            iteration: 1,
            wagons: { 
                '0-2': { 
                    iterationCounter: 1, 
                    sumCounter: 2, 
                    wagon: {
                        sum: 2,
                        nextGroupI: 0
                    }
                } 
            }
        };

        const res = countPeoples(roundMax, capacity, observation);

        expect(res).equal(20);
    })
    it("harder than easy", () => {
        const capacity = 5;
        const roundMax = 15;
        const observation = {
            cycleBeginAt: '4-5',
            sum: 36,
            iteration: 9,
            wagons: { 
                '0-5': {
                    iterationCounter: 1,
                    sumCounter: 5,
                    wagon: { sum: 5, nextGroupI: 2 }
                },
                '2-3': {
                    iterationCounter: 2,
                    sumCounter: 8,
                    wagon: { sum: 3, nextGroupI: 4 }
                },
                '4-5': {
                    iterationCounter: 3,
                    sumCounter: 13,
                    wagon: { sum: 5, nextGroupI: 6 }
                },
                '6-5': {
                    iterationCounter: 4,
                    sumCounter: 18,
                    wagon: { sum: 5, nextGroupI: 8 }
                },
                '8-2': {
                    iterationCounter: 5,
                    sumCounter: 20,
                    wagon: { sum: 2, nextGroupI: 9 }
                },
                '9-5': {
                    iterationCounter: 6,
                    sumCounter: 25,
                    wagon: { sum: 5, nextGroupI: 11 }
                },
                '11-5': {
                    iterationCounter: 7,
                    sumCounter: 30,
                    wagon: { sum: 5, nextGroupI: 1 }
                },
                '1-4': {
                    iterationCounter: 8,
                    sumCounter: 34,
                    wagon: { sum: 4, nextGroupI: 3 }
                },
                '3-2': {
                    iterationCounter: 9,
                    sumCounter: 36,
                    wagon: { sum: 2, nextGroupI: 4 }
                }
            }
        };

        const res = countPeoples(roundMax, capacity, observation);

        expect(res).equal(62);
    })
})