interface Wagon {
    sum: number;
    nextGroupI: number;
}

export const addToWagon = (capacity: number, groups: number[]) => {
    const recursive = (i: number = 0, firstIndiceIntoWagon: number = i, sum: number = 0): Wagon => {
        const tmpAdd = sum + groups[i]; // Add people into Wagon.

        if (tmpAdd > capacity) return { sum, nextGroupI: i } // oups, don't take it.

        let nextGroupI = i + 1 >= groups.length ? 0 : i + 1 
        if (nextGroupI === firstIndiceIntoWagon) return { sum: tmpAdd, nextGroupI } // next is already into this wagon

        return recursive(nextGroupI, firstIndiceIntoWagon, tmpAdd);
    }
    return recursive;
}

interface Observation {
    cycleBeginAt: string | null;
    sum: number;
    iteration: number,
    wagons: {
        [key: string]: {
            sumCounter: number;
            iterationCounter: number;
            wagon: Wagon
        }
    }
}

export const observeWagon = (capacity: number, roundMax: number, groups: number[]): Observation => {
    const createWagon = addToWagon(capacity, groups);
    const observation: Observation = {
        cycleBeginAt: null,
        sum: 0,
        iteration: 0,
        wagons: {}
    }

    const searchCycle = (groupI: number = 0): string | null => {
        if(observation.iteration > roundMax) return null // there is no cycle

        const wagon = createWagon(groupI);

        if(observation.wagons[`${groupI}-${wagon.sum}`]) return `${groupI}-${wagon.sum}` // first group in a same wagon capacity means cycle begin 
        observation.iteration ++

        // while cycle is undetermined, count.
        observation.sum += wagon.sum
        observation.wagons[`${groupI}-${wagon.sum}`] = {
            iterationCounter: observation.iteration,
            sumCounter: observation.sum,
            wagon
        }

        return searchCycle(wagon.nextGroupI);
    }

    observation.cycleBeginAt = searchCycle();
    
    return observation
}
    
export const countPeoples = (roundMax: number, capacity: number, observation: Observation) => {
    // Shift calcul is ugly, should be improve
    // Shift = count before begin of a cycle
    const shiftPeople = observation.cycleBeginAt === null ? 0 : observation.wagons[observation.cycleBeginAt].sumCounter - observation.wagons[observation.cycleBeginAt].wagon.sum;
    const shiftIteration = observation.cycleBeginAt === null ? 0 : observation.wagons[observation.cycleBeginAt].iterationCounter -1 ;

    // recurrence and number of cycle
    const peopleInACycle = observation.sum - shiftPeople;
    const iterationInACycle = observation.iteration - shiftIteration;
    const cycleInADay = Math.trunc((roundMax - shiftIteration) / iterationInACycle);

    // the last cycle maybe is not completed
    const wagonRestInADay = (roundMax - shiftIteration) % iterationInACycle
    const restWagonFromCycle = Object.keys(observation.wagons).find(k => observation.wagons[k].iterationCounter - shiftIteration === wagonRestInADay)
    const restPeople = restWagonFromCycle ? (observation.wagons[restWagonFromCycle].sumCounter - shiftPeople) : 0

    // the calcul
    return shiftPeople + cycleInADay * peopleInACycle + restPeople;
}


    