import app from './index'
import { expect } from 'chai'

const expectation = {
    "roller_coaster.hard": 8974489271113753,
    "roller_coaster.harder": 89744892714152289
}


describe("test", () => {
    it("should have good results for roller_coaster_hard", async () => {
        const test = 'roller_coaster.hard'
        const expectResult = expectation[test]
        
        const numberPeople = await app(`src/db/${test}`);

        expect(expectResult).equal(numberPeople)
    }) 
    it("should have good results for roller_coaster_harder", async () => {
        const test = 'roller_coaster.harder'
        const expectResult = expectation[test]
        
        const numberPeople = await app(`src/db/${test}`);

        expect(expectResult).equal(numberPeople)
    })
})