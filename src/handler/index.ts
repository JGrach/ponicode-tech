import { observeWagon, countPeoples } from './counter';
import { parser } from './parser';

export default async (input: string): Promise<number> => {
    const [limitUnit, limitIteration, group, entities] = await parser(input)
    const resolver = observeWagon(limitUnit, limitIteration, entities)
    return countPeoples(limitIteration, limitUnit, resolver)
}