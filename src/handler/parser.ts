import { promises as fsPromises} from 'fs'

const { readFile } = fsPromises;

// very ugly, just quick and dirty type fix:
const entitiesStringToNumber = (entities: string[]) => entities.map(entity => parseInt(entity))

export const parser = async (file: string): Promise<[number, number, number, number[]]> => {
    const db = await readFile(file, 'utf8')
    const entities = db.split('\n');
    const [limitUnit, limitIteration, entityLength] = entities.shift().split(' ');
    return [parseInt(limitUnit), parseInt(limitIteration), parseInt(entityLength), entitiesStringToNumber(entities)]
}

