import app from 'handler'

const input = process.argv[3] || 'src/db/roller_coaster.harder'

app(input).then((result) => {
    console.log(`People handled today: ${result}`)
}).catch(err => console.error(err));